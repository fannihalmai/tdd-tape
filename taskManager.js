const readline = require("readline");
const Task = require("./Task").Task
const prompt = require('prompt-sync')()
const Ui = require("./ui").Ui;


class TaskManager {
    constructor(){
        this.tasks = [];
        this.interactor = new ui();
    }

    parse(input){
        if(input){
            switch (input.charAt(0)){
                case '+':
                    this.addTask("desc");
                    break;
                case '-':
                    this.deleteTask();
                    break;
                default:
                    console.log("Not applicable");
            }
        }
    }

    addTask(desc){
        let id = this.tasks.length;
        this.tasks.push(new Task(id, desc)); // adds to array
    }

    print(){
        console.log(this.tasks);
    }

    run(){
        while(true){
            let input = this.interactor.read();
            if (input.charAt(0) === "q"){
                this.interactor.write("Quit program");
                break;
            }
            this.parse(input);
            this.interactor.write(this.tasks);
        }
    }
}

module.exports = {TaskManager}