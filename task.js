class Task {
    constructor(id, desc){
        this.id = id;
        this.description = desc;
        this.status = "pending";
    };

    complete(){
        this.status = "completed";
    }
}

module.exports = { Task };