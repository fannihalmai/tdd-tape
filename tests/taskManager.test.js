const test = require('tape')
const ui = require("../ui").Ui;
const Task = require("../task").Task;

class TaskManager {
    constructor(instructions){ // modified task manager not to read inputs from console 
        this.tasks = [];
        if(instructions){
            this.instructions = instructions;
        } else {
            this.instructions = [];
        }
        this.interactor = new ui();
    }

    parse(input){
        if(input){
            switch (input.charAt(0)){
                case '+':
                    this.addTask("desc");
                    break;
                case '-':
                    this.deleteTask();
                    break;
                case 'x':
                    this.completeTask();
                    break;
                default:
                    console.log("Not applicable");
            }
        }
    }

    completeTask(){
        this.tasks[0].complete();
    }

    addTask(desc){
        let id = this.tasks.length;
        this.tasks.push(new Task(id, desc)); // adds to array
    }

    run(){
        if(this.instructions.length === 0){ 
            this.interactor.write(["No tasks yet"]);
        } else {
            this.instructions.forEach(int=>{
                console.log(int);
                if (int.charAt(0) === "q"){
                    this.interactor.write("Quit program");
                } else {
                    this.parse(int);
                    this.interactor.write(this.tasks);
                }
            })
        }
    }
}

test('Empty task list when create task manager', (t) => {
    let emptyArray = [];
    let mgr = new TaskManager(emptyArray);
    t.equal(mgr.tasks.length, 0);
    t.end()
})

test('Adding task adds one task to list', (t) => {
    let mgr = new TaskManager(['+ task 1']);
    mgr.run();
    t.equal(mgr.tasks.length, 1);
    t.end()
})


test('Adding 2 tasks adds two task to list', (t) => {
    let mgr = new TaskManager(['+ task 1', '+ task 2']);
    mgr.run();
    t.equal(mgr.tasks.length, 2);
    t.end()
})

test('Completing a task maked it completed', (t) => {
    let mgr = new TaskManager(['+ task 1', 'x task 1']);
    mgr.run();
    t.ok(mgr.tasks[0].status === "completed");
    t.end()
})

test('Adding two tasks and complete second', (t) => {
    let mgr = new TaskManager(['+ task 1', '+ task 2', 'x task 2']);
    mgr.run();
    t.ok(mgr.tasks[1].status === "completed");
    t.end()
})


